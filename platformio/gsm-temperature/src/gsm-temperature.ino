// set > 0 to get debug serial output
#define DEBUG_MODE 1

// choose processor sleep mode (1 = on)
#define SLEEP_MODE_ON 1

#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <red_peg.h>
red_peg RP;
#include <teal_mallet_gsm.h>
teal_mallet_gsm TM;

// store each char array for datastream info as progmem strings
const char temp_param[] PROGMEM = "Temperature";
const char temp_quali[] PROGMEM = "1 min";
const char temp_dtype[] PROGMEM = "AIR";
const char temp_units[] PROGMEM = "C";
const char temp_perid[] PROGMEM = "INSTANT";

// then add the strings to an array (for multiple datastreams), also in progmem
PGM_P const param[] PROGMEM = {temp_param};
PGM_P const quali[] PROGMEM = {temp_quali};
PGM_P const dtype[] PROGMEM = {temp_dtype};
PGM_P const units[] PROGMEM = {temp_units};
PGM_P const perid[] PROGMEM = {temp_perid};

//PGM_P const all_readings PROGMEM = {param, quali, dtype, units, perid};

// operating states
enum state_machine_e {
  SLEEP_IDLE,
  TAKE_READING,
  SEND_READING
};
// start at idle
state_machine_e g_current_state = SLEEP_IDLE;

// readings are taken on ordinals (0/15/30/45)
t_SensorData last_reading_time;
#define ORDINAL_INTERVAL 1

#define NUM_MMNT 11 // measurement buffer length
// then create a pre-sized array of these structs to hold a set of timestamped measurements
uint8_t num_mmnt = NUM_MMNT;
t_measurement measurements[NUM_MMNT] = {};

uint8_t readings_head = 0;
uint8_t readings_tail = 0;

// sends happen after a fixed number of ordinals
t_SensorData last_sending_time;
// how many readings should be collected for sending (<NUM_MMNT)
#define NUM_TO_SEND 10

#if NUM_TO_SEND >= NUM_MMNT
#error "Can't hold all the readings, reduce NUM_TO_SEND below NUM_MMNT"
#endif

void setup() {
  RP.begin(true);
  TM.begin(((char*)"training.teal-mallet.net"), ((char*)"a6c33855-49a0-5858-8ffe-3da7c2b0412d"));
  delay(100);

  #if DEBUG_MODE > 0
  Serial.begin(BAUD);
  Serial.println(F("start EaMU purple-mallard"));
  #endif

  delay(500); // wait for red-peg to start up
  last_sending_time = get_reading(RTC);
  if (last_reading_time.y == 2165 or last_reading_time.y == 2000) {
    // the RTC has not been set or is missing, abort now
    Serial.println(F("RTC time is unset or missing"));
    pinMode(LED_BUILTIN, OUTPUT);
    while(true) {
      // die
      digitalWrite(LED_BUILTIN, HIGH);
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
    }
  }
  #if DEBUG_MODE > 0
  RP.print_data(last_reading_time);
  delay(100);
  #endif
}

void loop() {
  // lets just keep the main loop as clean as possible
  // driving state machine
  if (g_current_state == SLEEP_IDLE) {
    sleep_idle();
  } else if (g_current_state == TAKE_READING) {
    take_reading();
  } else if (g_current_state == SEND_READING) {
    send_reading();
  }
}

void sleep_idle() {
  // is it time to take a reading?
  #if SLEEP_MODE_ON > 0
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);             // select the watchdog timer mode
    MCUSR &= ~(1 << WDRF);                           // reset status flag
    WDTCSR |= (1 << WDCE) | (1 << WDE);              // enable configuration changes
    WDTCSR = (1<< WDP0) | (1 << WDP1) | (1 << WDP2); // set the prescalar = 7
    WDTCSR |= (1 << WDIE);                           // enable interrupt mode
    sleep_enable();                                  // enable the sleep mode ready for use
    #if DEBUG_MODE > 0
    Serial.print(F("sleep..."));
    delay(50);
    #endif
    // sleep is only for 4 seconds-ish
    sleep_mode();                                    // trigger the sleep
    /* ...time passes ... */
    sleep_disable();                                 // prevent further sleeps
    #if DEBUG_MODE > 0
    Serial.println(F("wake!"));
    #endif
  #else
    delay(4000); // simulate sleep
  #endif

  // get the time from the RTC
  t_SensorData current_time = get_reading(RTC);
  #if DEBUG_MODE > 0
  RP.print_data(current_time);
  delay(100);
  #endif
  if ((current_time.mm % ORDINAL_INTERVAL == 0)
      && (current_time.mm != last_reading_time.mm)) {
    // if it's an ordinal time 0|15|30|45 min take a reading
    g_current_state = TAKE_READING;
  } else if (((readings_head + NUM_MMNT) - readings_tail) % NUM_MMNT >= NUM_TO_SEND) {
    // the readings buffer is full, send them out
    g_current_state = SEND_READING;
  }
}

void take_reading() {
  // create the distance variable to hold the stage reading
  float temp = -50.0f; // start with OOR
  t_SensorData current_reading = get_reading(TMP);
  if (current_reading.sensor == TMP) {
    temp = RP.degC(current_reading);
  }
  #if DEBUG_MODE > 0
  Serial.print(F("take_reading() at: "));
  RP.print_data(current_reading);
  Serial.print(F("STAGE: "));
  Serial.println(temp, 3);
  delay(100);
  #endif

  // temperature should always be 150>temperature>-40
  if (temp >= -40.0f && temp <= 150.0f) {
    // add the reading to the buffer
    readings_head = (readings_head + 1) % NUM_MMNT;
    #if DEBUG_MODE > 0
    Serial.print(((readings_head + NUM_MMNT) - readings_tail) % NUM_MMNT);
    Serial.println(F(" readings stored"));
    #endif
    measurements[readings_head].reading = 0; // stage measurement
    measurements[readings_head].y = current_reading.y;
    measurements[readings_head].m = current_reading.m;
    measurements[readings_head].d = current_reading.d;
    measurements[readings_head].hh = current_reading.hh;
    measurements[readings_head].mm = current_reading.mm;
    measurements[readings_head].ss = current_reading.ss;
    measurements[readings_head].level = temp;

    // update the current time
    last_reading_time.y = current_reading.y;
    last_reading_time.m = current_reading.m;
    last_reading_time.d = current_reading.d;
    last_reading_time.hh = current_reading.hh;
    last_reading_time.mm = current_reading.mm;
    last_reading_time.ss = current_reading.ss;
    #if DEBUG_MODE > 0
  } else {
    Serial.println(F("failed temperature reading"));
    #endif
  }

  // and return to idle/sleep mode
  g_current_state = SLEEP_IDLE;
}

// we need to take readings a few times
t_SensorData get_reading(sensor_e target) {
  // get a reading
  RP.sensorsOn();
  if (target == ANA) {
    delay(160); // power up start ranging
    delay(148); // alow analog reading to settle
  } else {
    delay(100);
  }
  t_SensorData sensorData = RP.get(target);
  RP.sensorsOff();
  return sensorData;
}

void send_reading() {
  #if DEBUG_MODE > 0
  Serial.print(F("sending "));
  Serial.print(((readings_head + NUM_MMNT) - readings_tail) % NUM_MMNT);
  Serial.print(F(" readings ("));
  Serial.print(readings_tail);
  Serial.write(':');
  Serial.print(readings_head);
  Serial.println(F(")"));
  delay(100);
  #endif
  #if DEBUG_MODE > 0
  Serial.println(F("connecting GPRS..."));
  #endif
  TM.connectGPRS();
  #if DEBUG_MODE > 0
  Serial.println(F("startConnection..."));
  #endif
  TM.startConnection();
  #if DEBUG_MODE > 0
  Serial.println(F("sendReadings..."));
  #endif
  TM.sendReadings(&param[0], &quali[0], &dtype[0], &units[0], &perid[0], measurements, num_mmnt, readings_head, readings_tail);
  #if DEBUG_MODE > 0
  Serial.println(F("endConnection..."));
  #endif
  TM.endConnection();
  #if DEBUG_MODE > 0
  Serial.println(F("disconnectGPRS..."));
  #endif
  TM.disconnectGPRS();
  #if DEBUG_MODE > 0
  Serial.println(F("GPRS powered down."));
  #endif
  // reset the buffer length
  readings_tail = readings_head;

  // and go back to the main idle state
  g_current_state = SLEEP_IDLE;
}

ISR( WDT_vect ) {
  /* dummy */
}
