#ifndef teal_mallet_gsm_h
#define teal_mallet_gsm_h

// default data format is AMON JSON
#define AMON

#include "utility/GSM4.h"
#include "utility/JSONprint.h"

#define BAUD 115200

// set some default sim values
// we use O2 data sims in the adv-micros course
// PIN Number
#define PIN_NUMBER ((char*)"") // with no sim card pin
// APN data
#define GPRS_APN       ((char*)"mobile.o2.co.uk") // for O2
#define GPRS_LOGIN     ((char*)"o2web")    // for O2
#define GPRS_PASSWORD  ((char*)"password") // for O2

// use the AMON JSON format for sending
#define AMON

#define DEFAULT_PORT 80
#define DEFAULT_ENTITY_ID ((char*)"a3b40af1-16b5-4727-bcdc-60b0816a1e7b")

#define REPORTING_PERIOD 1000UL*60UL // one minute
#define RECORDING_PERIOD 1000UL*10UL // 10 seconds

#define SIG_FIG 3 // how many significant figures should we print for float readings?
#define SHORT_BUFF 20 // choose the json progmem output buffer length

typedef struct {
  char type[30]; // reading:type
  char unit[5]; // reading:unit
  char period[8]; // reading:period
} t_SensorType;

// create a struct to hold each reading
typedef struct{
  uint8_t reading; // reference which reading this if for (which index in the progmem arrays above)
  uint16_t y;
  uint8_t m, d, hh, mm, ss; //the timestamp in DateTime-ready format
  float level; // then the actual reading as a float, ready to send to teal-mallet
} t_measurement;

// timestamp
typedef struct{
  uint16_t y;
  uint8_t m, d, hh, mm, ss; //the timestamp in DateTime-ready format
} t_timestamp;

class teal_mallet_gsm
{
private:
  uint8_t _gsm_connected = false; // record if we're connected
  uint8_t _client_connection = false;

  // lets hold the connection information internally
  char* _server;
  int _port;
  char* _entityId; //e.g. "a3b40af1-16b5-4727-bcdc-60b0816a1e7b"
  char* _deviceId; // teal-mallet default

  // float to a char* utility
  // character string of set precision from float/double input
  char* ftoa(char *a, double f, int precision);

  int8_t buildAMON(PGM_P *param, PGM_P *quali, PGM_P *dtype, PGM_P *units, PGM_P *perid, t_measurement* the_measurements, uint8_t num_mmnt, uint8_t read_head, uint8_t read_tail);

public:
  //teal_mallet_gsm(Client& aClient);
  void begin(char* server, char* deviceId, char* entityId = DEFAULT_ENTITY_ID, int port = 80);

  uint8_t connectGPRS(char* _apn = GPRS_APN, char* _login = GPRS_LOGIN, char* _passwd = GPRS_PASSWORD, char* _pin = PIN_NUMBER);
  void disconnectGPRS();

  // call startConnection before sendOneReading
  uint8_t startConnection(); // make a client connection to the server
  // and call closeConnection to finish
  uint8_t endConnection(); // close that connection
  // send the hash and size headers, followed by a single reading
  // returns the http status code
  //int16_t sendOneReading(uint16_t _y, uint8_t _m, uint8_t _d, uint8_t _hh, uint8_t _mm, uint8_t _ss, double _float_reading, t_SensorType _sensor_type, bool send_to_serial = true);
  int16_t sendReadings(PGM_P *param, PGM_P *quali, PGM_P *dtype, PGM_P *units, PGM_P *perid, t_measurement* the_measurements, uint8_t num_mmnt, uint8_t read_head, uint8_t read_tail);
};

#endif
