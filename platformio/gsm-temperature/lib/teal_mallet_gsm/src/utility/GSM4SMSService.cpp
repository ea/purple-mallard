/*
This file is part of the GSM4 communications library for Arduino
-- Multi-transport communications platform
-- Fully asynchronous
-- Includes code for the Arduino-Telefonica GSM/GPRS Shield V1
-- Voice calls
-- SMS
-- TCP/IP connections
-- HTTP basic clients

This library has been developed by Telef�nica Digital - PDI -
- Physical Internet Lab, as part as its collaboration with
Arduino and the Open Hardware Community.

September-December 2012

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

The latest version of this library can always be found at
https://github.com/BlueVia/Official-Arduino
*/
#include "GSM4SMSService.h"
#include "GSM4MobileNetworkProvider.h"
#include <Arduino.h>

// While there is only a shield (ShieldV1) we will include it by default
#include "GSM4ShieldV1SMSProvider.h"
//GSM4ShieldV1SMSProvider theShieldV1SMSProvider;

#define GSM4SMSSERVICE_SYNCH 0x01 // 1: synchronous 0: asynchronous
#define __TOUT__ 10000


GSM4SMSService::GSM4SMSService(bool synch)
{
	if(synch)
		flags |= GSM4SMSSERVICE_SYNCH;
}

// Returns 0 if last command is still executing
// 1 if success
// >1 if error
int GSM4SMSService::ready()
{
	return theGSM4SMSProvider->ready();
}

int GSM4SMSService::beginSMS(const char *number)
{
	return waitForAnswerIfNeeded(theGSM4SMSProvider->beginSMS(number));
};

int GSM4SMSService::endSMS()
{
	return waitForAnswerIfNeeded(theGSM4SMSProvider->endSMS());
};

size_t GSM4SMSService::write(uint8_t c)
{
	theGSM4SMSProvider->writeSMS(c);
	return 1;
}

void GSM4SMSService::flush()
{
	theGSM4SMSProvider->flushSMS();
	waitForAnswerIfNeeded(1);
};

int GSM4SMSService::available()
{
	return waitForAnswerIfNeeded(theGSM4SMSProvider->availableSMS());
};

int GSM4SMSService::remoteNumber(char* number, int nlength)
{
	return theGSM4SMSProvider->remoteSMSNumber(number, nlength);

}

int GSM4SMSService::read()
{
	return theGSM4SMSProvider->readSMS();
};
int GSM4SMSService::peek()
{
	return theGSM4SMSProvider->peekSMS();
};

int GSM4SMSService::waitForAnswerIfNeeded(int returnvalue)
{
	// If synchronous
	if(flags & GSM4SMSSERVICE_SYNCH )
	{
		unsigned long m;
		m=millis();
		// Wait for __TOUT__
		while(((millis()-m)< __TOUT__ )&&(ready()==0))
			delay(100);
		// If everything was OK, return 1
		// else (timeout or error codes) return 0;
		if(ready()==1)
			return 1;
		else
			return 0;
	}
	// If not synchronous just kick ahead the coming result
	return ready();
}
