#include <inttypes.h>
#include "Arduino.h"
#include "JSONprint.h"

#include "DevNull.h"
DevNull devNull;

JSONprint::JSONprint() {
  _thePrint = &devNull;
}

void JSONprint::begin(Print &aPrint) {
  _thePrint = &aPrint;
  reset();
}

void JSONprint::beginNull() {
  begin(devNull);
}

void JSONprint::prettyMode(uint8_t pretty_mode) {
  _pretty_mode = pretty_mode;
}

size_t JSONprint::length() {
  return _char_count;
}

void JSONprint::reset() {
  for (uint8_t i=0; i<MAX_JSON_DEPTH; i++) {
    _state[i] = 0;
  }
  _depth = 0;
  _char_count = 0;
}

uint8_t JSONprint::openObject() {
  if (isObjectClosed()) {
    // we've just closed an object, so reset the status
    // but it's not the first object at this levet
    _state[_depth] = NOT_FIRST;
  }

  // check if the current object is open
  if (isObjectOpen()) {
    // it's already open, check that value is not started but the key is set
    if (isKeySet() && !isValueOpen()) {
      // setting an object in a value so increment depth
      _depth++;
      if (_depth >= MAX_JSON_DEPTH) {
        _depth = MAX_JSON_DEPTH;
        return 1; // failed (at max depth)
      } else {
        // clear the new status record and continue
        _state[_depth] = 0;
      }
    } else {
      return 1; // failed (not the right order)
    }
  }

  if (isNotFirst()) {
    // print a comma separator
    _char_count += _thePrint->write(',');
  }

  // open the new object
  _char_count += _thePrint->write('{');
  // record object open
  _state[_depth] = OBJECT_OPEN;

  return 0; // success
}

uint8_t JSONprint::writeKey(const __FlashStringHelper *ifsh) {
  if (canSetKey() == 0) {
    if (_pretty_mode) {
      _char_count += _thePrint->write('\n');
      for (uint8_t i=0; i<_depth+1; i++) {
        _char_count += _thePrint->write('\t');
      }
    }
    _char_count += _thePrint->write('\"');
    // output the progmem string byte by byte
    PGM_P p = reinterpret_cast<PGM_P>(ifsh);
    uint8_t n = 0;
    while (1) {
      unsigned char c = pgm_read_byte(p++);
      if (c == 0) break;
      if (_thePrint->write(c)) n++;
      else break;
    }
    _char_count += n;
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->write(':');
    if (_pretty_mode) {
      _char_count += _thePrint->write(' ');
    }
    _state[_depth] |= KEY_SET;
    return 0; // success
  }

  return 1; // failed: wrong order
}

uint8_t JSONprint::writeKey(char key[]) {
  if (canSetKey() == 0) {
    if (_pretty_mode) {
      _char_count += _thePrint->write('\n');
      for (uint8_t i=0; i<_depth+1; i++) {
        _char_count += _thePrint->write('\t');
      }
    }
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->print(key);
    _char_count += strlen(key);
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->write(':');
    _state[_depth] |= KEY_SET;
    return 0; // success
  }

  return 1; // failed: wrong order
}

uint8_t JSONprint::writeKey(char* key, size_t len) {
  if (canSetKey() == 0) {
    if (_pretty_mode) {
      _char_count += _thePrint->write('\n');
      for (uint8_t i=0; i<_depth+1; i++) {
        _char_count += _thePrint->write('\t');
      }
    }
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->write(key, len);
    _char_count += len;
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->write(':');
    _state[_depth] |= KEY_SET;
    return 0; // success
  }

  return 1; // failed: wrong order
}

uint8_t JSONprint::writeKey(char key) {
  if (canSetKey() == 0) {
    if (_pretty_mode) {
      _char_count += _thePrint->write('\n');
      for (uint8_t i=0; i<_depth+1; i++) {
        _char_count += _thePrint->write('\t');
      }
    }
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->write(key);
    _char_count += _thePrint->write('\"');
    _char_count += _thePrint->write(':');
    _state[_depth] |= KEY_SET;
    return 0; // success
  }

  return 1; // failed: wrong order
}

uint8_t JSONprint::openValue(uint8_t isString) {
  // what if we're in an array and don't need a key
  if (isArrayOpen() && !isArrayClosed() && isKeySet()) {
    // if we're not the first value
    if (isValueOpen() && isValueClosed()) {
      // reset the value flags
      _state[_depth] &= ~VALUE_OPEN;
      _state[_depth] &= ~VALUE_CLOSED;
      _char_count += _thePrint->write(',');
    }
  }

  if (isKeySet() && !isValueOpen()) {
    if (isString) {
      // begin the string
      _char_count += _thePrint->write('\"');
    }
    _state[_depth] |= VALUE_OPEN;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::closeValue(uint8_t isString) {
  if (isKeySet() && isValueOpen() && !isValueClosed()) {
    if (isString) {
      // end the string
      _char_count += _thePrint->write('\"');
    }
    _state[_depth] |= VALUE_CLOSED;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(const __FlashStringHelper *ifsh) {
  if (canSetValue() == 0) {
    // output the progmem string byte by byte
    PGM_P p = reinterpret_cast<PGM_P>(ifsh);
    uint8_t n = 0;
    while (1) {
      unsigned char c = pgm_read_byte(p++);
      if (c == 0) break;
      if (_thePrint->write(c)) n++;
      else break;
    }
    _char_count += n;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(char value[]) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->print(value);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(char* value, size_t len) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->write(value, len);
    _char_count += len;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(char value) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->write(value);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(long value) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->print(value);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(unsigned long value) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->print(value);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(int value) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->print(value);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(unsigned int value) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->print(value);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::appendValue(double value, int digits) {
  if (canSetValue() == 0) {
    _char_count += _thePrint->print(value, digits);
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(const __FlashStringHelper *ifsh) {
  if (openValue(true) == 0 && appendValue(ifsh) == 0 && closeValue(true) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(char value[]) {
  if (openValue(true) == 0 && appendValue(value) == 0 && closeValue(true) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(char* value, size_t len) {
  if (openValue(true) == 0 && appendValue(value, len) == 0 && closeValue(true) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(char value) {
  if (openValue(true) == 0 && appendValue(value) == 0 && closeValue(true) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(int value) {
  if (openValue(false) == 0 && appendValue(value) == 0 && closeValue(false) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(unsigned int value) {
  if (openValue(false) == 0 && appendValue(value) == 0 && closeValue(false) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(long value) {
  if (openValue(false) == 0 && appendValue(value) == 0 && closeValue(false) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(unsigned long value) {
  if (openValue(false) == 0 && appendValue(value) == 0 && closeValue(false) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::writeValue(double value, int digits) {
  if (openValue(false) == 0 && appendValue(value, digits) == 0 && closeValue(false) == 0) {
    return 0; //success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::openArray() {
  if (isKeySet() && !isValueOpen()) {
    _char_count += _thePrint->write('[');
    _state[_depth] |= ARRAY_OPEN;
    _state[_depth] &= ~ARRAY_CLOSED;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::closeArray() {
  if ( (!isArrayOpen() && _depth > 0) || (isArrayOpen() && isArrayClosed() && _depth > 0) ) {
    // we don't have an array open here, or it's already closed; try the previous level
    if ((_state[_depth-1] & ARRAY_OPEN) == ARRAY_OPEN && (_state[_depth-1] & ARRAY_CLOSED) != ARRAY_CLOSED) {
      _depth--;
    }
  }
  // must have array open, and not be part way through a value
  if (isArrayOpen() && !isArrayClosed() && ((isValueOpen() << 1) - isValueClosed() == 0)) {
    _char_count += _thePrint->write(']');
    _state[_depth] |= ARRAY_CLOSED;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::closeObject(uint8_t moveUp) {
  if (isObjectClosed()) {
    if (moveUp == true) {
        // reduce the level, as this one is already closed
        if (_depth == 0) {
        return 1; // failed, we've already closed the root object
      } else {
        _depth--;
      }
    } else {
      // remain idempotent unless specifically instructed
      return 1; // already closed at this level
    }
  }

  if(isObjectOpen() && ((isValueOpen() << 1) - isValueClosed()) == 0 && ((isArrayOpen() << 1) - isArrayClosed()) == 0) {
    if (_pretty_mode) {
      _char_count += _thePrint->write('\n');
      for (uint8_t i=0; i<_depth; i++) {
        _char_count += _thePrint->write('\t');
      }
    }
    _char_count += _thePrint->write('}');
    _state[_depth] |= OBJECT_CLOSED;
    return 0; // success
  }
  return 1; // failed: wrong time
}

uint8_t JSONprint::closeAll() {
  while (closeValue() == 0 || closeArray() == 0 || closeObject(true) == 0) {
    // keep going while there are open objects
  }
  return 0;
}


///// Private functions /////

uint8_t JSONprint::canSetKey() {
  // if key and value are both set at this level,
  // this is not the first key, so use a comma
  if (isKeySet() && (isValueClosed() || (isArrayOpen() && isArrayClosed()))) {
    // reset KEY_SET, VALUE_OPEN and VALUE_CLOSED
    _state[_depth] &= ~KEY_SET;
    _state[_depth] &= ~VALUE_OPEN;
    _state[_depth] &= ~VALUE_CLOSED;
    _char_count += _thePrint->write(',');
  }

  if (!isKeySet()) {
    return 0; // success
  }
  return 1; // wrong time
}

uint8_t JSONprint::canSetValue() {
  if (isKeySet() && !isValueOpen()) {
    openValue(true);
  }
  if (isKeySet() && isValueOpen() && !isValueClosed()) {
    // output the string
    return 0; // fine
  }
  return 1; // wrong time
}
