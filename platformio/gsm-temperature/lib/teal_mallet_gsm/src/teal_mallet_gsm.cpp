#include <inttypes.h>
#include "Arduino.h"

#include "teal_mallet_gsm.h"
JSONprint json;
GPRS Gprs;
GSM GsmAccess;
GSMClient Client;

#define DEBUG_MODE 1

//teal_mallet_gsm::teal_mallet_gsm(Client& aClient) :Client(aClient)
//{
//}

void teal_mallet_gsm::begin(char* server, char* deviceId, char* entityId, int port) {
  _server = server;
  _port = port;
  _entityId = entityId;
  _deviceId = deviceId;
}

//int teal_mallet_gsm::post(char *entityId, char *deviceId, t_SensorType reading_vars, int one_data)
//{
  //HttpClient http(Client);
//
//}

uint8_t teal_mallet_gsm::connectGPRS(char* _apn, char* _login, char* _passwd, char* _pin)
{
  while (!_gsm_connected) {
    digitalWrite(3,HIGH);
    if(GsmAccess.begin(_pin)==GSM_READY) {
      delay(3000);
      if(Gprs.attachGPRS(_apn, _login, _passwd)==GPRS_READY) {
        _gsm_connected = true;
        return true;
      }
    } else {
      delay(1000);
    }
  }
  return 0;
}

void teal_mallet_gsm::disconnectGPRS()
{
  while(_gsm_connected) {
    if(GsmAccess.shutdown()) {
      delay(1000);
      //digitalWrite(3,LOW);
      _gsm_connected = false;
    } else {
      delay(1000);
    }
  }
}

uint8_t teal_mallet_gsm::startConnection()
{
  if (Client.connect(_server, _port)) {
    _client_connection = true;
    // Make a HTTP request:
    Client.print(F("POST "));
    // path
    Client.print(F("/api/"));
    Client.print(_entityId);
    Client.print(F("/station/"));
    Client.print(_deviceId);
    Client.print(F("/measurements"));
    // http version
    Client.println(F(" HTTP/1.1"));
    // host line
    Client.print(F("Host: "));
    Client.println(_server);
    // required headers:
    Client.println(F("Connection: close"));
    Client.println(F("Content-Type: application/json"));
    return true;
  } else {
    return false;
  }
}

uint8_t teal_mallet_gsm::endConnection()
{
  // if there's a connection, stop the Client:
  if (_client_connection == true) {
    while (Client.available()) {
      Client.read();
    }
    Client.stop();
    // update the state
    _client_connection = true;
    return true;
  } else {
    // we're not connected
    return false;
  }
}

char* teal_mallet_gsm::ftoa(char *a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char *ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}

int16_t teal_mallet_gsm::sendReadings(PGM_P *param, PGM_P *quali, PGM_P *dtype, PGM_P *units, PGM_P *perid, t_measurement* the_measurements, uint8_t num_mmnt, uint8_t read_head, uint8_t read_tail)
{
  if (_client_connection == true) {
    // create the body dynamically and send after length header
    Print* s;
    #if DEBUG_MODE > 0
    for (int i=0; i<2; i++) {
      if (i == 0) {
        // first time client
        s = &Serial;
      } else {
        s = &Client;
      }
    #else
    for (int i=0; i<1; i++) {
      s = &Client;
    #endif

      // then send the actual headers
      s->print(F("POST "));
      s->print(F("/api/"));
      s->print(F("a3b40af1-16b5-4727-bcdc-60b0816a1e7b"));
      s->print(F("/station/"));
      s->print(F("a6c33855-49a0-5858-8ffe-3da7c2b0412d"));
      s->print(F("/measurements"));
      s->println(F(" HTTP/1.1"));
      s->print(F("Host: "));
      s->println(_server);
      s->println(F("Connection: close"));
      s->println(F("Content-Type: application/json"));
      s->print(F("Content-length: ")); // need to calculate the content length before sending, so:
      // start the jsonPrint with null output (discard the actual output characters)
      json.beginNull();
      #if DEBUG_MODE > 0
      // (optional) print the JSON nicely with tabs and spaces 
      json.prettyPrint();
      #endif
      // build the json array (outputting to nowhere)
      buildAMON(&param[0], &quali[0], &dtype[0], &units[0], &perid[0], the_measurements, num_mmnt, read_head, read_tail);
      // then send how many bytes that took
      s->println(json.length());
      // finish the headers with a blank line
      s->println();
      // then restart jsonPrint with a new target (sending to client)
      json.begin(*s);
      // build the json
      buildAMON(&param[0], &quali[0], &dtype[0], &units[0], &perid[0], the_measurements, num_mmnt, read_head, read_tail);
      // finish the last line of the message (jsonPrint won't press return)
      s->println();
    }

    // wait for any incoming data
    unsigned long timeoutStart = millis();
    uint16_t char_counter = 0;
    uint16_t status_code = 0;
    while ( (Client.connected() || Client.available()) && ( (millis() - timeoutStart) < 30000UL) ) {
      if (Client.available()) {
        char c = Client.read();
        char_counter++;
        // Print out this character
        #if DEBUG_MODE > 0
          Serial.print(c);
        #endif
        if (char_counter > 9 && char_counter < 13) {
          // TODO: we should probably check that we've got numbers too!
          //chars 10, 11 & 12 form the status_code
          status_code = (status_code * 10) + (c - '0');
        }
        // We read something, reset the timeout counter
        timeoutStart = millis();
      } else {
        // We haven't got any data, so let's pause to allow some to arrive
        delay(1000);
      }
    }
    return status_code;
  } else {
    // if you didn't get a connection to the server:
    return -1;
  }
}

int8_t teal_mallet_gsm::buildAMON(PGM_P *param, PGM_P *quali, PGM_P *dtype, PGM_P *units, PGM_P *perid, t_measurement* the_measurements, uint8_t num_mmnt, uint8_t read_head, uint8_t read_tail)
{
  int8_t measurements_built = 0;
  // calculate the number of readings
  uint8_t numReadings = sizeof(param)/sizeof(*param);
  // calculate the number of measurements
  #if DEBUG_MODE > 1
  Serial.print(F("num_mmnt: "));
  Serial.println(num_mmnt);
  delay(100);
  #endif

 json.openObject();
  json.writeKey(F("devices"));
    json.openArray();
    json.openObject();
      json.writeKey(F("deviceId"));
      json.writeValue(F("a6c33855-49a0-5858-8ffe-3da7c2b0412d"));
      json.writeKey(F("entityId"));
      json.writeValue(F("a3b40af1-16b5-4727-bcdc-60b0816a1e7b"));
      json.writeKey(F("readings"));
      json.openArray();
      char buff[SHORT_BUFF]; // need a short buffer to cast the progmem strings into
      // send all reading metadata (even if we don't use it - because who cares)
      for (int i=0; i<sizeof(numReadings); i++) {
        json.openObject();
          json.writeKey(F("type"));
          json.openValue();
            strcpy_P(buff, (char*)pgm_read_word(&(param[i])));
            json.appendValue(buff);
            json.appendValue(F(":"));
            strcpy_P(buff, (char*)pgm_read_word(&(quali[i])));
            json.appendValue(buff);
            json.appendValue(F(":"));
            strcpy_P(buff, (char*)pgm_read_word(&(dtype[i])));
            json.appendValue(buff);
          json.closeValue();
          json.writeKey(F("unit"));
          strcpy_P(buff, (char*)pgm_read_word(&(units[i])));
          json.writeValue(buff);
          json.writeKey(F("period"));
          strcpy_P(buff, (char*)pgm_read_word(&(perid[i])));
          json.writeValue(buff);
        json.closeObject();
      }
      json.closeArray();
      json.writeKey(F("measurements"));
      json.openArray();
      //send each measurement in the array
      while (read_tail != read_head) {
        // increment the read_tail
        read_tail = (read_tail + 1) % num_mmnt;
        #if DEBUG_MODE > 1
        Serial.print(F("HEAD: "));
        Serial.println(read_head);
        Serial.print(F("TAIL: "));
        Serial.println(read_tail);
        delay(20);
        #endif
        // then build each measurements json
        json.openObject();
          json.writeKey(F("type"));
          json.openValue();
            strcpy_P(buff, (char*)pgm_read_word(&(param[the_measurements[read_tail].reading])));
            json.appendValue(buff);
            json.appendValue(F(":"));
            strcpy_P(buff, (char*)pgm_read_word(&(quali[the_measurements[read_tail].reading])));
            json.appendValue(buff);
            json.appendValue(F(":"));
            strcpy_P(buff, (char*)pgm_read_word(&(dtype[the_measurements[read_tail].reading])));
            json.appendValue(buff);
          json.closeValue();
          json.writeKey(F("timestamp"));
          // write out the ISO8061 datetime from the measurements array
          json.openValue();
            sprintf(buff, "%04d", the_measurements[read_tail].y);
            json.appendValue(buff);
            json.appendValue('-');
            sprintf(buff, "%02d", the_measurements[read_tail].m);
            json.appendValue(buff);
            json.appendValue('-');
            sprintf(buff, "%02d", the_measurements[read_tail].d);
            json.appendValue(buff);
            json.appendValue('T');
            sprintf(buff, "%02d", the_measurements[read_tail].hh);
            json.appendValue(buff);
            json.appendValue(':');
            sprintf(buff, "%02d", the_measurements[read_tail].mm);
            json.appendValue(buff);
            json.appendValue(':');
            sprintf(buff, "%02d", 0); // time should be on the ordinal so round down
            json.appendValue(buff);
            json.appendValue('Z');
          json.closeValue();
          json.writeKey(F("value"));
          json.openValue(false);
          ftoa(buff, the_measurements[read_tail].level, SIG_FIG);
          json.appendValue(buff);
          json.closeValue(false);
        json.closeObject();

        // increment the measurement counter
        measurements_built++;
      }
      json.closeArray();
    json.closeObject();
    json.closeArray();
 json.closeObject();

 return measurements_built;
}
