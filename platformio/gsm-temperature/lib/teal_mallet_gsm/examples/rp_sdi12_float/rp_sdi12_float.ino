#include <red_peg.h>
red_peg RP;

void setup() {
  RP.begin();
  delay(100);
  Serial.begin(BAUD);
  Serial.println("starting rp_sdi12_float");

}

void loop() {
  RP.sensorsOn();
  delay(100);
  // make a first ask for a sensor
  t_SensorData genbox = RP.ask(SDI_12_FLOAT);
  // wait for the SDI12 to return (expected time)
  delay(4100);
  // then go through the incoming buffer to find a response
  for (int i=0; i<=BUFFER_LEN; i++) {
    if (genbox.sensor == SDI_12_FLOAT) {
      //RP.print_data(genbox);
      Serial.print(genbox.y);
      Serial.print("-");
      Serial.print(genbox.m);
      Serial.print("-");
      Serial.print(genbox.d);
      Serial.print("T");
      Serial.print(genbox.hh);
      Serial.print(":");
      Serial.print(genbox.mm);
      Serial.print(":");
      Serial.print(genbox.ss);
      Serial.print("Z, ");
      // SDI_12_FLOAT request returns the reported float level (eg. meters)
      Serial.print(genbox.float_level);
      Serial.println();
      //stop looping
      i = BUFFER_LEN+1;
    } else {
      // wait a bit
      delay(500);
      // ask for nothing to see if we get the original reply
      genbox = RP.ask(OK);
    }
  }

  RP.sensorsOff();
  delay(5000);
}
