#include <red_peg.h>
red_peg RP;

void setup()
{
  Serial.begin(115200);
  Serial.println(F("starting rp_set_time"));

  RP.begin();
  t_SensorData the_time = RP.get(RTC);
  Serial.print(F("time now: "));
  Serial.print(the_time.y);
  Serial.print("-");
  Serial.print(the_time.m);
  Serial.print("-");
  Serial.print(the_time.d);
  Serial.print(" T ");
  Serial.print(the_time.hh);
  Serial.print(":");
  Serial.print(the_time.mm);
  Serial.print(":");
  Serial.print(the_time.ss);
  Serial.print("Z");
  Serial.println();

  Serial.println(F("To set the time send \"T YYYY MM DD hh mm s\""));
}

void loop()
{
  if (Serial.available()) {
    // if we have serial input, check input for setting the time
    setTime();
  }
}

void setTime() {
  char c = Serial.read();
  if (c == 't' || c == 'T') {
    uint16_t year;
    uint8_t month, day, hour, min, sec;
    Serial.print(F("Year: "));
    year = Serial.parseInt();
    Serial.println(year);
    Serial.print(F("Month: "));
    month = Serial.parseInt();
    Serial.println(month);
    Serial.print(F("Day: "));
    day = Serial.parseInt();
    Serial.println(day);
    Serial.print(F("Hour: "));
    hour = Serial.parseInt();
    Serial.println(hour);
    Serial.print(F("Minute: "));
    min = Serial.parseInt();
    Serial.println(min);
    Serial.print(F("Second: "));
    sec = Serial.parseInt();
    Serial.println(sec);
    RP.ask(SET_RTC, year, month, day, hour, min, sec);
    t_SensorData the_time = RP.get(RTC);
    
    Serial.print(F("RTC set to: "));
    Serial.print(the_time.y);
    Serial.print("-");
    Serial.print(the_time.m);
    Serial.print("-");
    Serial.print(the_time.d);
    Serial.print(" T ");
    Serial.print(the_time.hh);
    Serial.print(":");
    Serial.print(the_time.mm);
    Serial.print(":");
    Serial.print(the_time.ss);
    Serial.print("Z");
    Serial.println();
  }
}

