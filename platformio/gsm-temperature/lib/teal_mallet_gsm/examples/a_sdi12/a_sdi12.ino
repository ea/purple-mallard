/*
 * Read an SDI12 genbox sensor attached to the red-peg board

  Modified from an example written by Kevin M. Smith in 2013.
  Contact: SDI12@ethosengineering.org
*/

#define BAUD 115200
#define RP_SDI_PIN 9  // Red Peg SDI-12 is on pin 9

#include <SDI12.h>
SDI12 mySDI12(RP_SDI_PIN);
#include <red_peg.h>
red_peg RP;


char sdi12_address = '0';

//String myCommand = "?I!"; // wildcard for infomation from all sensors
//String myCommand = "0D0!"; // get readings for sensor '0'
//String myCommand = "0M!"; // how many measurments, and wait time for sensor '0'

void setup() {
  delay(1000);
  RP.begin();
  delay(100);
  // turn on the sensors 
  RP.sensorsOn();
  delay(1000);

  // start the serial monitor
  Serial.begin(BAUD);
  Serial.println(F("starting a_sdi12"));


  // start the SDI12 connection
  mySDI12.begin();
  delay(300); // allow things to settle
  bool foundSdi = false;
  // check addresses 0-9
  if (foundSdi == false) {
    for (char i = '0'; i <= '9'; i++) {
      if (checkActive(i)) {
        sdi12_address = i;
        foundSdi = true;
        i = '9' + 1;
      }
    }
  }
  // check addresses a-z
  if (foundSdi == false) {
    for (char i = 'a'; i <= 'z'; i++) {
      if (checkActive(i)) {
        sdi12_address = i;
        foundSdi = true;
        i = '9' + 1;
      }
    }
  }
  // check addresses A-Z
  if (foundSdi == false) {
    for (char i = 'A'; i <= 'Z'; i++) {
      if (checkActive(i)) {
        sdi12_address = i;
        foundSdi = true;
        i = '9' + 1;
      }
    }
  }

  // report the first sensor found to the serial monitor
  if (foundSdi) {
    Serial.print(F("Found SDI device at address: "));
    Serial.println(sdi12_address);
  } else {
    Serial.println(F("No SDI device found"));
    //die
    while(1){}
  }
  delay(500);

  // clear the incoming character buffer
  while(mySDI12.available()){    
    mySDI12.read();
  }
}

void loop() {
  char i = sdi12_address;
  String command = "";

  // get the measurements
  command += i;
  command += "M!"; // SDI-12 measurement command format  [address]['M'][!]
  Serial.print("sending: ");
  Serial.println(command);
  mySDI12.sendCommand(command);
  while (!mySDI12.available() > 5); // wait for acknowlegement with format [address][ttt (3 char, seconds)][number of measurments available, 0-9]
  delay(300);
  
  // uncomment the next 3 lines to write the response message to Serial
  
  //while(mySDI12.available()){    
    //Serial.write(mySDI12.read());
  //}
  
  char addr = mySDI12.read(); //consume the address
  Serial.print("Address: ");
  Serial.println(addr);

  // find out how long we have to wait (in seconds).
  int wait = 0;
  wait += 100 * (mySDI12.read() - '0');
  wait += 10 * (mySDI12.read() - '0');
  wait += 1 * (mySDI12.read() - '0');
  // lets make sure it's a sensible wait
  wait = constrain(wait, 1, 10);
  Serial.print("Wait for: ");
  Serial.print(wait);
  Serial.println(" seconds");


  // find the number of measurements
  int num_measurements = mySDI12.read() - '0';
  Serial.print("Measurements available: ");
  Serial.println(num_measurements);

  mySDI12.read(); // ignore carriage return '\r'
  mySDI12.read(); // ignore line feed '\n'

  // flush any remaining characters
  while(mySDI12.available()){ 
    mySDI12.read();
  }
  delay(100);
  
  // in this example we will only take the 'DO' measurement
  command = "";
  command += i;
  /* for the GenBox signal generator:
      "D0!" will give the "current" measurement
      "D1!" will return two values for each measurement
  */
  command += "D0!"; // SDI-12 command to get data [address][D][dataOption][!]
  Serial.print("sending: ");
  Serial.println(command);
  mySDI12.sendCommand(command);
  while (!mySDI12.available() > 1); // wait for acknowlegement
  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000UL * wait)) {
    // wait the specified amount of time
  }
  
  // uncomment the next 3 lines to write the response message to Serial
  
  //while(mySDI12.available()){    
    //Serial.write(mySDI12.read());
  //}

  mySDI12.read(); // ignore address return
  mySDI12.read(); // ignore the '+' spacer character

  // SDI12 lib doesn't have a parse float function
  // we'll have to do it manually
  float val1 = 0;
  bool pre_dp = true;
  int decimal_place = 1;
  for (int i = 0; i < 10; i++) {
    if (mySDI12.available()) {
      char c = mySDI12.read();
      // if it's a number
      if (c >= '0' && c <= '9') {
        if (pre_dp == true) {
          // if we haven't crossed the decimal place
          // multiply the current value by 10 and add the new number
          val1 = (val1 * 10) + (c - '0');
        } else {
          // divide the new number into the number of decimal places
          // and add to the current value
          val1 = val1 + (float(c - '0') / (10.0 * (float)decimal_place));
          decimal_place++;
        }
      } else if (c == '.') {
        // flag that we've crossed the decimal place
        pre_dp = false;
      } else {
        // we've reached the end of the number
        // so stop checking characters
        break;
      }
    }
  }

  // the GenBox gives floats to 4 decimal places, *1000 to make a milli reading
  long returnval = long(val1 * 1000.0);
  Serial.print(returnval);
  Serial.println(" mmWG ");

  // or print the float value
  //Serial.print(val1);
  //Serial.println(" mWG ");
  
  //mySDI12.flush();
  while(mySDI12.available()) {    
    mySDI12.read();
  }
  Serial.println("end");
  
  delay(5000); // run through again in five seconds
}

// this checks for activity at a particular SDI-12 address
// expects a char, '0'-'9', 'a'-'z', or 'A'-'Z'
boolean checkActive(char i) {

  String myCommand = "";
  myCommand = "";
  myCommand += (char) i;                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (int j = 0; j < 3; j++) {          // goes through three rapid contact attempts
    mySDI12.sendCommand(myCommand);
    if (mySDI12.available() > 1) break;
    delay(30);
  }
  if (mySDI12.available() > 2) {   // if it hears anything it assumes the address is occupied
    mySDI12.flush();
    return true;
  }
  else {   // otherwise it is vacant.
    mySDI12.flush();
  }
  return false;
}


