#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>

#define BAUD 115200 

void setup() {
  Serial.begin(BAUD);
  Serial.println("start sleepy");
  DDRD &= B00000011;       // set Arduino pins 2 to 7 as inputs, leaves 0 & 1 (RX & TX) as is
  DDRB = B00000000;        // set pins 8 to 13 as inputs
  PORTD |= B11111100;      // enable pullups on pins 2 to 7, leave pins 0 and 1 alone
  PORTB |= B11111111;      // enable pullups on pins 8 to 13
  pinMode(13,OUTPUT);      // set pin 13 as an output so we can use LED to monitor
}

void loop() {
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  Serial.print("sleeping...zzzzz...");
  delay(100); // to let the serial buffer send
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);             // select the watchdog timer mode
  MCUSR &= ~(1 << WDRF);                           // reset status flag
  WDTCSR |= (1 << WDCE) | (1 << WDE);              // enable configuration changes
  WDTCSR = (1<< WDP0) | (1 << WDP1) | (1 << WDP2); // set the prescalar = 7
  WDTCSR |= (1 << WDIE);                           // enable interrupt mode
  sleep_enable();                                  // enable the sleep mode ready for use
  sleep_mode();                                    // trigger the sleep
  /* ...time passes ... */
  sleep_disable();                                 // prevent further sleeps
  Serial.println("awake!");
}

ISR( WDT_vect ) {
  /* dummy */
}
