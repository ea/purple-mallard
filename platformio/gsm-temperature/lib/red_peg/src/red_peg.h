/* Red-Peg master and slave library

  allows easier (smaller overhead) reading of red-peg
  sensors by the attached microcontroller board, using
  the onboard ATmega328 on the red-peg shield
*/
#ifndef red_peg_h
#define red_peg_h

#include <SPI.h>
#include <Wire.h>
// set the slave's buffer lengths
#define BUFFER_LEN 5
// set the red-peg filename format
#define FILENAME_DAT "RP%06u.dat"

// control pin definitions
#define SENSOR_ACTIVE_PIN 6
#define SD_SS 5
#define RP_SDI_PIN 9

// set the default baud rate
#ifndef BAUD
#define BAUD 115200
#endif

#define START_BYTE 0xFF
#define END_BYTE 0x00
#define TRANSFER_DELAY 25 // µs spi inter-byte delay
#define PROCESSING_DELAY 10 // no. ms to allow for the slave to process the request

// possible inputs for the_command or the_sensor
enum sensor_e {EMPTY, AVAILABLE, OK, RTC, ADC1, ADC2, ADC3, SDI_12, SET_RTC, SDI_12_FLOAT, SDI_12_SCAN, VERSION};
// and some aliases relavent for the red-peg shield
#define MA4_20 ADC1
#define ANA ADC2
#define TMP ADC3

// possible ultrasound sensors
enum usound_e {MB7060, MB7366};

// min and max values for 4-20mA readings
// ADC max is 2.048V
#ifndef MIN_4MA
#define MIN_4MA 6400L // theoretical 0.4V
#endif
#ifndef MAX_20MA
#define MAX_20MA 31999L // theoretical 2.0V
#endif

typedef struct{
  uint8_t start_byte = START_BYTE;
  union {
    // aliases for the sensor/command variable
    sensor_e the_sensor;
    sensor_e sensor;
    sensor_e the_command;
    sensor_e command;
  };
  uint16_t y;
  uint8_t m, d, hh, mm, ss; //the timestamp in DateTime-ready format
  union {
    // some aliases for the data reading
    int32_t the_reading;
    int32_t reading;
    int32_t value;
    int32_t integer;
    float float_level; // currently unused
    union { // return semantic version number in 4-bytes
      uint8_t major;
      uint8_t minor;
      uint8_t patch;
      uint8_t metadata; // normally zero
    };
  };
  //float the_data; // replaced with functions
  uint8_t end_byte = END_BYTE;
} t_SensorData;

class red_peg
{
private:
  volatile uint8_t _tx_buffer_head;
  volatile uint8_t _tx_buffer_tail;
  volatile uint8_t _rx_buffer_head;
  volatile uint8_t _rx_buffer_tail;

  bool _sd_write = false;
  uint8_t _ss_pin;
  uint8_t _get_timeout = 4;
  void printHex(byte* data, int data_len);

public:
  const size_t size_t_SensorData = sizeof(t_SensorData);

  // set up the red-peg library requirements (call during setup())
  void begin(bool sd_write = false, uint8_t ss_pin = 8, bool report_serial = true);
  // use RP.ask(REQUEST) to retrieve the value from the requested sensor
  t_SensorData ask(sensor_e request = EMPTY, uint8_t _y = END_BYTE, uint8_t _m = END_BYTE, uint8_t _d = END_BYTE, uint8_t _hh = END_BYTE, uint8_t _mm = END_BYTE, uint8_t _ss = END_BYTE, uint16_t _the_reading = END_BYTE); //, float _the_data = 0.0);
  // use RP.get(REQUEST) to queue a request for a reading
  // use RP.get(EMPTY) until you get the response (up to BUFFER_LEN+1 times)
  t_SensorData get(sensor_e request);
  // prints the data_record to Serial
  void print_data(t_SensorData data_record);
  // Serial.print the human readable version of request
  char* sensor_message(sensor_e request); // return a human readable version of the enum

  // sensor control
  void sensorsOn(); // turn the external sensors on
  void sensorsOff(); // turn the external sensors off

  // Helper functions for calculating values
  float degC(t_SensorData data_record); // TMP26 reading in degrees C
  float volts(t_SensorData data_record); // Analog voltage reading for ADC1/2/3
  float mA(t_SensorData data_record); // current conversion for 4-20mA sensor
  // level sensor conversion using either integer or float
  long level(t_SensorData data_record, int max_level);
  long level(t_SensorData data_record, long max_level);
  float level(t_SensorData data_record, double max_level);
  float level(t_SensorData data_record, float max_level);
  // TODO:
  long distance(t_SensorData data_record, usound_e sensor_type = MB7060); // Ultrasonic distance reading (and sensor type)
};

#endif // red_peg_h
