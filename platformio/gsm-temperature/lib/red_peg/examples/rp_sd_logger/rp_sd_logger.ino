#include <SPI.h>
#include <SdFat.h>
SdFat SD;
File myFile;
#include <red_peg.h>
red_peg RP;

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(BAUD);
  Serial.println("starting rp_sd_logger");
  RP.begin();

  Serial.print("Initializing SD card...");
  pinMode(10, OUTPUT);
  pinMode(SD_SS, OUTPUT);

  if (!SD.begin(SD_SS)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
}

void loop()
{
  RP.sensorsOn();
  delay(100);
  t_SensorData temp = RP.get(TMP);
  RP.sensorsOff();
  if (temp.sensor == TMP) {
    myFile = SD.open("readings.csv", FILE_WRITE);
    if (myFile) {
      Serial.print("Writing to readings.csv: ");

      // write the timestamp to the file
      myFile.print(temp.y);
      myFile.print("-");
      myFile.print(temp.m);
      myFile.print("-");
      myFile.print(temp.d);
      myFile.print("-");
      myFile.print("T");
      myFile.print(temp.hh);
      myFile.print(":");
      myFile.print(temp.mm);
      myFile.print(":");
      myFile.print(temp.ss);
      myFile.print("Z");
      // plus a comma delimeter
      myFile.print(", ");
      // followed by the temperature
      myFile.print(RP.degC(temp));
      // and a new line
      myFile.println();
      
      // close the file:
      myFile.close();
      
      // Then print what we sent to the Serial port
      RP.print_data(temp);
      Serial.println("done.");

      // and wait 5 seconds
      delay(5000);
    }
  }
}


