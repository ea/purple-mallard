#include <red_peg.h>
red_peg RP;


void setup() {
  RP.begin();
  RP.sensorsOn();
  delay(100);
  Serial.begin(BAUD);
  Serial.println("starting rp_version_check");
}

void loop() {
  t_SensorData version_info = RP.get(VERSION);
  if (version_info.sensor == VERSION) {
    Serial.print("RP library version: ");
    Serial.print(version_info.major);
    Serial.print(".");
    Serial.print(version_info.minor);
    Serial.print(".");
    Serial.print(version_info.patch);
    Serial.println();
  } else {
    Serial.print("no return from red-peg-slave: ");
    RP.print_data(version_info);
  }
  delay(5000);
}
