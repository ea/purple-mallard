#include <red_peg.h>
red_peg RP;

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(BAUD);
  Serial.println(F("start rp_sensor_active"));
  RP.begin();
}

// the loop function runs over and over again forever
void loop() {
  RP.sensorsOn();
  delay(1000);              // wait for a second
  RP.sensorsOff();
  delay(1000);              // wait for a second
}
