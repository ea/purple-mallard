#define SENSOR_ACTIVE_PIN 6
#define BAUD 115200

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(BAUD);
  Serial.println(F("start sensor_blink"));
  delay(100);
  pinMode(SENSOR_ACTIVE_PIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(SENSOR_ACTIVE_PIN, HIGH);
  delay(1000);              // wait for a second
  digitalWrite(SENSOR_ACTIVE_PIN, LOW);
  delay(1000);              // wait for a second
}
