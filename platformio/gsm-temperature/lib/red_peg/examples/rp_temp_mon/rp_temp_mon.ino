#include <red_peg.h>
red_peg RP;


void setup() {
  Serial.begin(BAUD);
  RP.begin();
  RP.sensorsOn();
  delay(100);
}

void loop() {
  t_SensorData temp = RP.get(TMP);
  if (temp.sensor == TMP) {
    Serial.print("Value: ");
    Serial.print(temp.reading);
    Serial.print(", ");
    Serial.print(RP.volts(temp));
    Serial.print("V, ");
    Serial.print(RP.degC(temp));
    Serial.print(" degC");
    Serial.println();
  } else {
    Serial.println("no return from TMP");
  }
  delay(5000);
}
