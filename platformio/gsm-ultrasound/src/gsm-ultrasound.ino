#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <red_peg.h>
red_peg RP;
#include <teal_mallet_gsm.h>
teal_mallet_gsm TM;
#include <SPI.h>
#include <SdFat.h>
SdFat SD;
File myFile;
#define SD_CS_PIN SD_SS

/* settings: */
#define MAX_DEPTH 15000 // sensor to bed depth in mm
#define OFFSET 0 // gauge offset in mm

#define ORDINAL_INTERVAL 15 // ordinal minutes (0,15,30,45)
#define NUM_MMNT 11 // measurement buffer length
#define NUM_TO_SEND 10 // how many measurements should be gathered before sending

#define SERVER_NAME ((char*)"nw.teal-mallet.net")
#define ENTITY_ID ((char*)"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa")
/* end settings */

// set > 0 to get debug serial output
#define DEBUG_MODE 0

// set to 0 to remove gsm sending
#define USE_GSM 1

// choose processor sleep mode (1 = on)
#define SLEEP_MODE_ON 1

// store each char array for datastream info as progmem strings
const char level_param[] PROGMEM = "Level";
const char level_quali[] PROGMEM = "15 min";
const char level_dtype[] PROGMEM = "STAGE";
const char level_units[] PROGMEM = "m";
const char level_perid[] PROGMEM = "INSTANT";

// then add the strings to an array (for multiple datastreams), also in progmem
PGM_P const param[] PROGMEM = {level_param};
PGM_P const quali[] PROGMEM = {level_quali};
PGM_P const dtype[] PROGMEM = {level_dtype};
PGM_P const units[] PROGMEM = {level_units};
PGM_P const perid[] PROGMEM = {level_perid};

// operating states
enum state_machine_e {
  SLEEP_IDLE,
  TAKE_READING,
  SEND_READING
};
// start at idle
state_machine_e g_current_state = SLEEP_IDLE;

// readings are taken on ordinals (0/15/30/45)
t_SensorData last_reading_time;
// then create a pre-sized array of these structs to hold a set of timestamped measurements
uint8_t num_mmnt = NUM_MMNT;
t_measurement measurements[NUM_MMNT] = {};

uint8_t readings_head = 0;
uint8_t readings_tail = 0;

// sends happen after a fixed number of ordinals
t_SensorData last_sending_time;
// how many readings should be collected for sending (<NUM_MMNT)

#if NUM_TO_SEND >= NUM_MMNT
#error "Can't hold all the readings, reduce NUM_TO_SEND below NUM_MMNT"
#endif

void setup() {
  RP.begin(true);
  #if USE_GSM > 0
  TM.begin(SERVER_NAME, ENTITY_ID);
  #endif
  delay(100);

  Serial.begin(BAUD);
  #if DEBUG_MODE > 1
  Serial.println(F("start EaMU purple-mallard"));
  #endif

  delay(500); // wait for red-peg to start up
  last_sending_time = get_reading(RTC);
  if (last_reading_time.y == 2165 or last_reading_time.y == 2000) {
    // the RTC has not been set or is missing, abort now
    #if DEBUG_MODE > 1
    Serial.println(F("Bad RTC"));
    #endif
    pinMode(LED_BUILTIN, OUTPUT);
    while(true) {
      // die
      digitalWrite(LED_BUILTIN, HIGH);
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
    }
  }
  #if DEBUG_MODE > 1
  RP.print_data(last_reading_time);
  delay(100);
  #endif

  // test SD card present
  if (!SD.begin(SD_CS_PIN)) {
    #if DEBUG_MODE > 1
    Serial.println(F("No SD"));
    #endif
    return;
  }
  #if DEBUG_MODE > 1
  Serial.println(F("SD OK"));
  #endif

  // die here
  //while(true){}
}

void loop() {
  // lets just keep the main loop as clean as possible
  // driving state machine
  if (g_current_state == SLEEP_IDLE) {
    sleep_idle();
  } else if (g_current_state == TAKE_READING) {
    take_measurement();
  } else if (g_current_state == SEND_READING) {
    send_reading();
  }
}

void sleep_idle() {
  // is it time to take a reading?
  #if SLEEP_MODE_ON > 0
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);             // select the watchdog timer mode
    MCUSR &= ~(1 << WDRF);                           // reset status flag
    WDTCSR |= (1 << WDCE) | (1 << WDE);              // enable configuration changes
    WDTCSR = (1<< WDP0) | (1 << WDP1) | (1 << WDP2); // set the prescalar = 7
    WDTCSR |= (1 << WDIE);                           // enable interrupt mode
    sleep_enable();                                  // enable the sleep mode ready for use
    #if DEBUG_MODE > 0
      #if DEBUG_MODE > 1
      Serial.print(F("sleep..."));
      #else
      Serial.print(F("s."));
      #endif
    delay(50);
    #endif
    // sleep is only for 4 seconds-ish
    for (int i=0; i<12; i++) {
      sleep_mode();                                    // trigger the sleep
      /* ...time passes ... */
    }
    sleep_disable();                                 // prevent further sleeps
    #if DEBUG_MODE > 0
      #if DEBUG_MODE > 1
      Serial.println(F("wake!"));
      #else
      Serial.println(F("!"));
      #endif
    #endif
  #else
    #if DEBUG_MODE > 0
      Serial.print("s.");
    #endif
    delay(4000); // simulate sleep
    #if DEBUG_MODE > 0
      Serial.println("!");
    #endif
  #endif

  // get the time from the RTC
  t_SensorData current_time = get_reading(RTC);
  #if DEBUG_MODE > 1
  RP.print_data(current_time);
  delay(100);
  #endif
  if ((current_time.mm % ORDINAL_INTERVAL == 0)
      && (current_time.mm != last_reading_time.mm)) {
    // if it's an ordinal time 0|15|30|45 min take a reading
    g_current_state = TAKE_READING;
  } else if (((readings_head + NUM_MMNT) - readings_tail) % NUM_MMNT >= NUM_TO_SEND) {
    // the readings buffer is full, send them out
    g_current_state = SEND_READING;
  }
}

void take_measurement() {
  // create the distance variable to hold the stage reading
  float temp = -1.0; // start with OOR
  t_SensorData current_reading = get_reading(ANA);
  if (current_reading.sensor == ANA) {
    // if it's the correct reading, calculate the output
    temp = float(MAX_DEPTH - RP.distance(current_reading, MB7366) - OFFSET) / 1000.0;
  }
  #if DEBUG_MODE > 1
  Serial.print(F("take mmnt at: "));
  RP.print_data(current_reading);
  Serial.print(F("STAGE: "));
  #endif
  #if DEBUG_MODE > 0
  Serial.println(temp, 3);
  delay(100);
  #endif

  // stage calculation should always return positive
  if (temp >= 0.0) {
    // add the reading to the buffer
    readings_head = (readings_head + 1) % NUM_MMNT;
    #if DEBUG_MODE > 0
    Serial.print(((readings_head + NUM_MMNT) - readings_tail) % NUM_MMNT);
    #endif
    #if DEBUG_MODE > 1
    Serial.print(F(" mmnts stored"));
    #endif
    #if DEBUG_MODE > 0
    Serial.println();
    #endif

    // add the reading to the measurements buffer
    measurements[readings_head].reading = 0; // stage measurement
    measurements[readings_head].y = current_reading.y;
    measurements[readings_head].m = current_reading.m;
    measurements[readings_head].d = current_reading.d;
    measurements[readings_head].hh = current_reading.hh;
    measurements[readings_head].mm = current_reading.mm;
    measurements[readings_head].ss = current_reading.ss;
    measurements[readings_head].level = (float)temp;

    // write to the open SD card file
    char buff[7];
    // change directory to root
    SD.chdir("/");
    // create the current folder path
    sprintf(buff, "%04d%02d", current_reading.y, current_reading.m);
    if (!SD.exists(buff)) {
      // make new directory if absent
      SD.mkdir(buff);
    }
    // change to YYYYMM/
    SD.chdir(buff);
    // create the day file name
    sprintf(buff, "%02d.csv", current_reading.d);
    uint8_t file_exists = true;
    // check if it's a new file and needs headers
    if (!SD.exists(buff)) {
      file_exists = false;
    }
    // open DD.log file
    myFile = SD.open(buff, FILE_WRITE);
    if (file_exists == false) {
      // write headers
      myFile.println(F("date,time,stage(m)"));
    }
    myFile.print(current_reading.y);
    myFile.write('-');
    if (current_reading.m < 10) { myFile.write('0'); }
    myFile.print(current_reading.m);
    myFile.write('-');
    if (current_reading.d < 10) { myFile.write('0'); }
    myFile.print(current_reading.d);
    myFile.write(',');
    if (current_reading.hh < 10) { myFile.write('0'); }
    myFile.print(current_reading.hh);
    myFile.write(':');
    if (current_reading.mm < 10) { myFile.write('0'); }
    myFile.print(current_reading.mm);
    myFile.write(':');
    myFile.print("00"); // zero seconds
    myFile.write(',');
    myFile.print(temp, 3); // stage in m
    myFile.println();
    myFile.close();

    // update the current time
    last_reading_time.y = current_reading.y;
    last_reading_time.m = current_reading.m;
    last_reading_time.d = current_reading.d;
    last_reading_time.hh = current_reading.hh;
    last_reading_time.mm = current_reading.mm;
    last_reading_time.ss = current_reading.ss;
    #if DEBUG_MODE > 1
  } else {
    Serial.println(F("OOR"));
    #endif
  }

  // and return to idle/sleep mode
  g_current_state = SLEEP_IDLE;
}

// we need to take readings a few times
t_SensorData get_reading(sensor_e target) {
  // get a reading
  RP.sensorsOn();
  if (target == ANA) {
    delay(160); // power up start ranging
    delay(148); // alow analog reading to settle
  }
  delay(100);
  t_SensorData incoming;
  if (target == ANA) {
    incoming = RP.ask(target);
    //delay(30000); // wait for 30 seconds for 40 reads to be averaged
    delay(4000);
    for (uint8_t i=0; i<5+1; i++) {
      // send an empty get
      incoming = RP.ask();
      if (incoming.the_sensor == target) {
        RP.sensorsOff();
        return incoming;
      }
      delay(10);
    }
    // if we did'nt get the right answer, set empty
    incoming.the_sensor = EMPTY;
  } else {
    // normal target, normal get()
    incoming = RP.get(target);
  }
  RP.sensorsOff();
  return incoming;
}

void send_reading() {
  #if DEBUG_MODE > 1
  Serial.print(F("sending "));
  Serial.print(((readings_head + NUM_MMNT) - readings_tail) % NUM_MMNT);
  Serial.print(F(" readings ("));
  Serial.print(readings_tail);
  Serial.write(':');
  Serial.print(readings_head);
  Serial.println(F(")"));
  delay(100);
  #endif
#if USE_GSM > 0
  #if DEBUG_MODE > 1
  Serial.println(F("connecting GPRS..."));
  #endif
  TM.connectGPRS();
  #if DEBUG_MODE > 1
  Serial.println(F("startConnection..."));
  #endif
  TM.startConnection();
  #if DEBUG_MODE > 1
  Serial.println(F("sendReadings..."));
  #endif
  TM.sendReadings(&param[0], &quali[0], &dtype[0], &units[0], &perid[0], measurements, num_mmnt, readings_head, readings_tail);
  #if DEBUG_MODE > 1
  Serial.println(F("endConnection..."));
  #endif
  TM.endConnection();
  #if DEBUG_MODE > 1
  Serial.println(F("disconnectGPRS..."));
  #endif
  TM.disconnectGPRS();
  #if DEBUG_MODE > 1
  Serial.println(F("GPRS powered down."));
  #endif
#endif
  // reset the buffer length
  readings_tail = readings_head;

  // and go back to the main idle state
  g_current_state = SLEEP_IDLE;
}

ISR( WDT_vect ) {
  /* dummy */
}
