#include <red_peg.h>
#include <teal_mallet_gsm.h>
red_peg RP;
teal_mallet_gsm TM;

// PIN Number
#define PINNUMBER ""

// APN data
#define GPRS_APN       "mobile.o2.co.uk" // replace your GPRS APN
#define GPRS_LOGIN     "o2web"    // replace with your GPRS login
#define GPRS_PASSWORD  "password" // replace with your GPRS password

const char deviceId[] = "00000000-0000-0000-0000-000000000000";
const char entityId[] = "a3b40af1-16b5-4727-bcdc-60b0816a1e7b"; //37chars
const char privacy[] = "public";

// URL, path & port (for example: arduino.cc)
const char server[] = "training.teal-mallet.net";
int port = 80; // port 80 is the default for HTTP
// POST to /api/<entity_id>/station/<id>/measurements

#define RECORDING_PERIOD 60000UL // 60 seconds

const t_SensorType tempDatastream = {"Temp:1 min:TMP", "C", "INSTANT"};
const t_SensorType levelDatastream = {"Level:1 min:4-20mA", "mm", "INSTANT"};

t_SensorData tempReading; // id of the sensor from SensorType array
t_SensorData levelReading; // id of the sensor from SensorType array

uint32_t last_record = -RECORDING_PERIOD; // send data immediately

// initialize the library instance
GPRS gprs;
GSM gsmAccess;
GSMClient client;

void setup()
{
  Serial.begin(BAUD);
  Serial.println(F("start rp_send_temp"));
  delay(500);
  RP.begin();

  // connection state
  boolean notConnected = true;
  // After starting the modem with GSM.begin()
  // attach the shield to the GPRS network with the APN, login and password
  while (notConnected) {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

}

void loop()
  // check the existing files and build the next filename

{
  // check if we've reached the next time to record a reading
  if (millis() - last_record >= RECORDING_PERIOD) {
    client.stop();
    Serial.println(F("sending"));
    RP.sensorsOn();
    delay(100);
    tempReading = RP.get(TMP);
    levelReading = RP.get(MA4_20);
    RP.sensorsOff();
    if (tempReading.sensor == TMP && levelReading.sensor == MA4_20) {
      Serial.print(tempReading.y);
      Serial.write('-');
      Serial.print(tempReading.m);
      Serial.write('-');
      Serial.print(tempReading.d);
      Serial.write('T');
      Serial.print(tempReading.hh);
      Serial.write(':');
      Serial.print(tempReading.mm);
      Serial.write(':');
      Serial.print(tempReading.ss);
      Serial.print("Z, ");
      Serial.print(RP.degC(tempReading));
      Serial.println(F(" degC"));

      Serial.print(levelReading.y);
      Serial.write('-');
      Serial.print(levelReading.m);
      Serial.write('-');
      Serial.print(levelReading.d);
      Serial.write('T');
      Serial.print(levelReading.hh);
      Serial.write(':');
      Serial.print(levelReading.mm);
      Serial.write(':');
      Serial.print(levelReading.ss);
      Serial.print("Z, ");
      Serial.print(RP.level(levelReading, 7000));
      Serial.println(F(" degC"));

      last_record = millis();

      Serial.println("connecting...");
      if (client.connect(server, port)) {

        Serial.println(F("HTTP_REQ"));
        // Make a HTTP request:
        client.print(F("POST "));
        // path
        client.print(F("/api/"));
        client.print(entityId);
        client.print(F("/station/"));
        client.print(deviceId);
        client.print(F("/measurements"));
        // http version
        client.println(F(" HTTP/1.1"));
        // host line
        client.print(F("Host: "));
        client.println(server);
        // required headers:
        client.println(F("Connection: close"));
        client.println(F("Content-Type: application/json"));

        // need a larger char array to hold all the outgoing body
        char body[500];
        strcpy(body, "{\"devices\":[");
        strcat(body, "{\"deviceId\":\"");
        strcat(body, deviceId);
        strcat(body, "\",\"entityId\":\"");
        strcat(body, entityId);
        strcat(body, "\",\"readings\":[");
        strcat(body, "{\"type\":\"");
        strcat(body, tempDatastream.type);
        strcat(body, "\",\"unit\":\"");
        strcat(body, tempDatastream.unit);
        strcat(body, "\",\"period\":\"");
        strcat(body, tempDatastream.period);
        strcat(body, "\"},");
        strcat(body, "{\"type\":\"");
        strcat(body, levelDatastream.type);
        strcat(body, "\",\"unit\":\"");
        strcat(body, levelDatastream.unit);
        strcat(body, "\",\"period\":\"");
        strcat(body, levelDatastream.period);
        strcat(body, "\"}");
        strcat(body, "],\"measurements\":[");
        strcat(body, "{\"type\": \"");
        strcat(body, tempDatastream.type);
        strcat(body, "\",\"timestamp\":\"");
        char buff[8];
        sprintf(buff, "%04d", tempReading.y);
        strcat(body, buff);
        strcat(body, "-");
        sprintf(buff, "%02d", tempReading.m);
        strcat(body, buff);
        strcat(body, "-");
        sprintf(buff, "%02d", tempReading.d);
        strcat(body, buff);
        strcat(body, "T");
        sprintf(buff, "%02d", tempReading.hh);
        strcat(body, buff);
        strcat(body, ":");
        sprintf(buff, "%02d", tempReading.mm);
        strcat(body, buff);
        strcat(body, ":");
        sprintf(buff, "%02d", tempReading.ss);
        strcat(body, buff);
        strcat(body, "Z");
        strcat(body, "\",\"value\":");
        ftoa(buff, RP.degC(tempReading), 3);
        strcat(body, buff);
        strcat(body, "},");
        strcat(body, "{\"type\": \"");
        strcat(body, levelDatastream.type);
        strcat(body, "\",\"timestamp\":\"");
        sprintf(buff, "%04d", levelReading.y);
        strcat(body, buff);
        strcat(body, "-");
        sprintf(buff, "%02d", levelReading.m);
        strcat(body, buff);
        strcat(body, "-");
        sprintf(buff, "%02d", levelReading.d);
        strcat(body, buff);
        strcat(body, "T");
        sprintf(buff, "%02d", levelReading.hh);
        strcat(body, buff);
        strcat(body, ":");
        sprintf(buff, "%02d", levelReading.mm);
        strcat(body, buff);
        strcat(body, ":");
        sprintf(buff, "%02d", levelReading.ss);
        strcat(body, buff);
        strcat(body, "Z");
        strcat(body, "\",\"value\":");
        ftoa(buff, RP.level(levelReading, 7000), 3);
        strcat(body, buff);
        strcat(body, "}]}]}");

        //calculate content length
        client.print(F("Content-Length: "));
        client.println(strlen(body));
        client.println(); // newline to separate the headers
        client.println(body); // then print the body
        client.println();
        Serial.println(body);
      } else {
        // if you didn't get a connection to the server:
        Serial.println(F("CONN_FAIL"));
      }
    } else {
      RP.print_data(tempReading);
      RP.print_data(tempReading);
    }
  }

  while (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  // if the server's disconnected, stop the client:
  if (!client.available() && !client.connected()) {
    client.stop();
  }

}

char *ftoa(char *a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char *ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}
