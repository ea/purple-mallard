/* Blink without delay
 *  
 */

#define BAUD 115200

bool blinkState = true;
unsigned long lastBlink = 0UL;
unsigned long lastFizz = 0UL;
unsigned long lastBuzz = 0UL;

void setup() {
  Serial.begin(BAUD);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  // flash the light every second
  if (millis() - lastBlink >= 1000) {
    digitalWrite(LED_BUILTIN, blinkState);
    Serial.print(millis()/1000);
    Serial.println(" blink");
    blinkState = !blinkState;
    lastBlink = millis();
  }

  // print fizz every 3 seconds
  if (millis() - lastFizz >= 3000) {
    Serial.print(millis()/1000);
    Serial.println(" fizz");
    lastFizz = millis();
  }
  
  // print buzz every 7 seconds
  if (millis() - lastBuzz >= 7000) {
    Serial.print(millis()/1000);
    Serial.println(" buzz");
    lastBuzz = millis();
  }

}
