/*
  JSONprint.h — A library for streaming out JSON
  By building dynamically while sending, the RAM usage is
  minimised, at the expense of additional processing time
  if the JSON data is used repeatedly

  Created by Def-Proc Engineering 2017
  MIT licensed
 */
#include <Arduino.h>
#ifndef JSONprint_h
#define JSONprint_h

#define MAX_JSON_DEPTH 8

// define the bitwise flags for keeping track of progress
#define OBJECT_OPEN   B00000001
#define NOT_FIRST     B00000010
#define KEY_SET       B00000100
#define VALUE_OPEN    B00001000
#define VALUE_CLOSED  B00010000
#define ARRAY_OPEN    B00100000
#define ARRAY_CLOSED  B01000000
#define OBJECT_CLOSED B10000000

class JSONprint
{
private:
  uint8_t _state[MAX_JSON_DEPTH];
  uint8_t _depth = 0;

  // TODO: set pretty mode to add linebreaks
  uint8_t _pretty_mode = false;
  uint16_t _char_count = 0;

  // set a default print stream
  Print* _thePrint;

  uint8_t isObjectOpen() { return (_state[_depth] & OBJECT_OPEN); };
  uint8_t isNotFirst() { return (_state[_depth] & NOT_FIRST); };
  uint8_t isKeySet() { return (_state[_depth] & KEY_SET); };
  uint8_t isValueOpen() { return (_state[_depth] & VALUE_OPEN); };
  uint8_t isValueClosed() { return (_state[_depth] & VALUE_CLOSED); };
  uint8_t isArrayOpen() { return (_state[_depth] & ARRAY_OPEN); };
  uint8_t isArrayClosed() { return (_state[_depth] & ARRAY_CLOSED); };
  uint8_t isObjectClosed() { return (_state[_depth] & OBJECT_CLOSED); };

  // return err val for position set
  uint8_t canSetKey();
  uint8_t canSetValue();

public:
  JSONprint();

  // set a new stream output
  void begin(Print &aPrint = Serial);

  // begin with no output (counting only)
  void beginNull();

  // set pretty mode
  void prettyMode(uint8_t pretty_mode = true);
  void prettyPrint(uint8_t pretty_mode = true) { return prettyMode(pretty_mode); }

  // how many characters in the json string
  size_t length();

  // open a new object
  uint8_t openObject();

  // write a double quoted string key with trailing colon
  uint8_t writeKey(const __FlashStringHelper *);
  uint8_t writeKey(char key[]);
  uint8_t writeKey(char* key, size_t len);
  uint8_t writeKey(char key);

  // write a complete value.
  uint8_t writeValue(const __FlashStringHelper *);
  uint8_t writeValue(char value[]);
  uint8_t writeValue(char* value, size_t len);
  uint8_t writeValue(char value);
  uint8_t writeValue(int value);
  uint8_t writeValue(unsigned int value);
  uint8_t writeValue(long value);
  uint8_t writeValue(unsigned long value);
  uint8_t writeValue(double value, int digits = 2);

  // open a Value for a more multipart write
  uint8_t openValue(uint8_t isString = true);

  // append new values to an open value
  uint8_t appendValue(const __FlashStringHelper *);
  uint8_t appendValue(char value[]);
  uint8_t appendValue(char* value, size_t len);
  uint8_t appendValue(char value);
  uint8_t appendValue(int value);
  uint8_t appendValue(unsigned int value);
  uint8_t appendValue(long value);
  uint8_t appendValue(unsigned long value);
  uint8_t appendValue(double value, int digits = 2);

  // close a written value
  uint8_t closeValue(uint8_t isString = true);

  // open and close an array
  uint8_t openArray();
  uint8_t closeArray();

  // close an object
  uint8_t closeObject(uint8_t moveUp = false);

  // helper function to just close everything
  // will try and close value, array and objects in order
  uint8_t closeAll();

  // reset the JSON object
  void reset();
};

#endif
