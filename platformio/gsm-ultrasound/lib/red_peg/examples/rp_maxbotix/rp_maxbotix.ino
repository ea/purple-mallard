/*
 * Reading the MB 7060 sensor to give distance readings
 * 
 * It is recommended to providing an external power supply
 * as the maxbotix ultrasound sensors can draw more than 
 * the 0.5A that a USB port is able to supply
 * 
 * Powering from USB only may lead to some crashes/restarts
 * because of the current draw
 * 
 */
#include <red_peg.h>
red_peg RP;


void setup() {
  delay(2000);
  RP.begin();

  Serial.begin(BAUD);
  Serial.println("starting rp_maxbotix");

}

void loop() {
  RP.sensorsOn();
  // wait long enough for the ultrasound to start up
  delay(500);
  t_SensorData depth = RP.get(ANA);
  RP.sensorsOff();
  
  if (depth.sensor == ANA) {
    RP.print_data(depth);
    Serial.print("Value: ");
    Serial.print(depth.reading);
    Serial.print(", ");
    Serial.print(RP.volts(depth));
    Serial.print(" V, ");
    Serial.print(RP.distance(depth));
    Serial.print(" cm");
    Serial.println();
    delay(5000);
  }
}
