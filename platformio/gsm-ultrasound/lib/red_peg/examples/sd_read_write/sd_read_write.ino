/*
  SD card read/write with Red-Peg

 This example shows how to read and write data to and from an SD card file
 The circuit:
 * the SD card is attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 5

 modified from sketch by 
 David A. Mellis & Tom Igoe
 */

#include <SPI.h>
#include <SdFat.h>
SdFat SD;
File myFile;

#include <red_peg.h>
red_peg RP;

#define SD_CS_PIN 5 

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(BAUD); // 115200
  RP.begin();

  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output
  // or the SD library functions will not work.
  pinMode(10, OUTPUT);
  pinMode(SD_CS_PIN, OUTPUT);

  if (!SD.begin(SD_CS_PIN)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
    // write a line of data to the sdcard
    myFile.println("testing 1, 2, 3.");
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  // re-open the file for reading:
  myFile = SD.open("test.txt");
  // check that worked fine
  if (myFile) {
    Serial.println("test.txt contains:");
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      char c = myFile.read();
      Serial.write(c);
    }
    // then close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}

void loop()
{
  // there's nothing here
}


