#include <red_peg.h>
red_peg RP;


void setup() {
  Serial.begin(BAUD);
  Serial.println("starting rp_ma4_20");
  RP.begin();
}

void loop() {
  RP.sensorsOn();
  delay(100);
  t_SensorData depth = RP.get(MA4_20);
  RP.sensorsOff();
  
  if (depth.sensor == MA4_20) {
    Serial.print("Value: ");
    Serial.print(depth.reading);
    Serial.print(", ");
    Serial.print(RP.mA(depth));
    Serial.print(" mA, ");
    Serial.print(RP.level(depth, 7000L));
    Serial.print(" mm");
    Serial.println();
  } else {
    Serial.println("no return from ADC");
  }
  delay(1000);
}
