# gsm-ultrasound

Arduino UNO sketch for collecting ultrasound stage readings from a red-peg shield, at ordinal times; then batch and send those readings to a teal-mallet server via an Arduino GSM shield.

The sketch assumes O2 sim card, the following lines will need to be added for another provider: 

```
// PIN Number
#define PIN_NUMBER ((char*)"") // with no sim card pin
// APN data
#define GPRS_APN       ((char*)"mobile.o2.co.uk") // for O2
#define GPRS_LOGIN     ((char*)"o2web")    // for O2
#define GPRS_PASSWORD  ((char*)"password") // for O2
```

## Settings

From the available settings:

Set the sensor to river bed distance, plus any static offset for the readings, in mm

```
#define MAX_DEPTH 15000 // sensor to bed depth in mm
#define OFFSET 0 // gauge offset in mm
```

The ordinal should be left at 15 minute multiples, and the maximum successful measurement buffer on the UNO is 11. The number to send must be less than the number of stored measurement, but can be reduced to send more frequently, at a cost to the battery life.

```
#define ORDINAL_INTERVAL 15 // ordinal minutes (0,15,30,45)
#define NUM_MMNT 11 // measurement buffer length
#define NUM_TO_SEND 10 // how many measurements should be gathered before sending
```

The server name is the receipient server address, and the entity id sets the station id to receive the readings. This `ENTITY_ID` *must* be unique for each device.

```
#define SERVER_NAME ((char*)"nw.teal-mallet.net")
#define ENTITY_ID ((char*)"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa")
```
