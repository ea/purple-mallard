![Purple Mallard](logo/purple_mallard.png)

An Arduino & red-peg based water-level ultrasound sensor in an IP rated case, battery, solar power and remote logging to a teal-mallet server via GSM Shield

# Changelog

 * TODO: Platformio code for gsm-ultrasound needs porting to compile in the
 Arduino IDE without library install.

## 20th November 2017

 * Sending changed to fractional meters instead of millimeters.
 * Settings moved to the top of the code, and server and entity_id separated out.
 * Offset distance added for adjusting the level.

## 19th November 2017

6 Prototypes prepared for in field testing, this is equivalent to TRL5. As
an initial test, the main target for this is to find as many problems
with the devices or design assumptions by placing the devices into a working
environment, and by them being used by people outside of the design process.

### Operation

The prototypes are set to record the distance reading from the ultrasound sensor
every 15 minutes, at 0, 15, 30 and 45 minutes past the hour. They convert the
distance reading into a simulated level reading (simulating a 15m sensor to bed
maximum) and record that stage measurement in millimetres with a date and time
stamp as comma separated values (CSV) file on the internal microSD card.

Measurements are stored as one file per day, with all the days of the month per
directory (`YYYYMM/DD.csv`). This folder usage limits the requirement to hold or
calculate a *current measurements* file name, and efficiently stores the files
to limit the number per folder so as not to exceed the limits of the FAT
partition format. With this nested structure, the file limits of the FAT format
should be able to store 21 years of information.

Measurements are sent out once 10 have been collected. While it would be preferable
to send out less frequently — as the power usage of the GSM modem is the greatest
part of the whole device usage — the sending rate is limited by the memory
capacity of the microcontroller chosen. This sending rate means measurements
sent remotely may be up to 2.5 hours old.

The device is powered on by opening the case and connecting the battery connector.
The device should then be closed fully and mounted for data collection. The
solar panel should be connected to the trailing lead from the case and mounted
at 45⁰ in a clear, south facing direction. At expected consumption rate of 4.5W/day,
and with the solar panel collecting an estimated 9W/day, the unit *should* be
able to run continuously. Without any solar charge, the 5000mAh battery is
capable of running the device for 4.5 days before it will shut down.

To power off the device fully, disconnect the solar charger or any DC charger
supply; then disconnect the battery.

It is possible to recharge the internal battery by connecting a 5–6V 2.1mm,
centre positive, DC supply to the jack socket on the solar charge board with the
case open. *Do not exceed 6V to this board.* The DC jack on the Arduino board
is not connected.

### Issues

The intention of these test units (numbered `0001`–`0006`) is to find as many
errors, problems and failures in operation and handling as possible. While it is
desirable to know of any successful operation, it is extremely valuable to
record any problems at this stage, so they can be fed back into product development.

Known issues include:

 * There is no record of battery voltage, so it is not possible to know
 remotely if a device goes offline if it's been caused by battery depletion.
 * The use of the GSM modem is the major power usage in the device, even in
 low power mode, the modem continuously uses more power than the sensor does
 when it is in use.
 * Having both GSM and SD recording uses a large amount of the available
 microcontroller RAM. It remains to be seen how important the local data
 collection is in practice, and if it can be removed in favour of remote only.
 * The solar collection is a noticeable cost in the unit. It is possible that
 with lower power requirements for sending — such as with an LPWAN solution —
 that it may be possible to run for extended periods without requiring an
 external energy supply.
 * While every effort has been made to ensure the solar collection meets the
 energy usage requirements of the unit, the amount of energy collected is highly
 dependent on the weather, location, orientation and view of the sky at the
 installation site.
